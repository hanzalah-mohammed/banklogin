package jdbc.dao;

public interface UserDaoInterface {
    boolean userAuthentication(String loggedAs, String userID, String userPassword);
}

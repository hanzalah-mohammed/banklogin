package jdbc.dao;

import utility.DBUtility;
import java.sql.*;

public class UserDaoImplementation implements UserDaoInterface{

    Connection refConnection = DBUtility.getConnection();
    PreparedStatement ps = null;
    ResultSet result = null;

    @Override
    public boolean userAuthentication(String loggedAs, String userID, String userPassword) {

        String sqlQuery; // query depends on user choice

        if (loggedAs.equals("admin")){
            sqlQuery = "SELECT * FROM admin WHERE userID = ?";
        } else {
            sqlQuery = "SELECT * FROM customer WHERE customerID = ?";
        }

        try {
            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setString(1, userID); // set the first ? as userID
            result = ps.executeQuery(); // Execute the query

            // Get the result from the query and store them in variables
            result.next();
            String resultID = result.getString(1);
            String resultPassword = result.getString(2);

            // Authenticate the user inputs with the result variables
            if (userID.equals(resultID) && userPassword.equals(resultPassword)) {
                return true;
            } else { // if the password does not match
                throw new Exception("Login Failed! Password is incorrect. \n");
            }
        } catch (SQLException e) { // if the SQL cannot run due to invalid input
            System.err.println("Login Failed! User '" + userID + "' does not exist. \n");
        } catch (Exception e) { // catch the thrown Exception
            System.err.println(e.getMessage());
        } // end of try and catch
        return false;
    }
}

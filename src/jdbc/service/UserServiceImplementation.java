package jdbc.service;

import jdbc.dao.UserDaoImplementation;
import jdbc.dao.UserDaoInterface;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserServiceImplementation implements UserServiceInterface{

    UserDaoInterface refInterface = new UserDaoImplementation();
    Scanner refScanner = new Scanner(System.in);
    String loggedAs = null;

    @Override
    public void userChoice() {

        boolean validChoice = false;

        while(!validChoice){
            try{
                System.out.println("Login as: \n 1) Admin \n 2) Customer \n");
                System.out.println("Please select one: ");
                int userChoice = refScanner.nextInt();

                if (userChoice == 1) {
                    loggedAs = "admin";
                    validChoice = true;
                } else if (userChoice == 2){
                    loggedAs = "customer";
                    validChoice = true;
                } else {
                    throw new Exception("Invalid input");
                }
                userLogin();
            } catch (InputMismatchException e){
                System.err.println("Invalid Input! Please restart your session");
                break;
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void userLogin() {

        boolean userAuthenticated = false;

        while (!userAuthenticated){
            System.out.println("Enter " + loggedAs + " ID: ");
            String userID = refScanner.next();

            System.out.println("Enter password: ");
            String userPassword = refScanner.next();

            if(refInterface.userAuthentication(loggedAs, userID, userPassword)){
                userAuthenticated = true;
                System.out.println("Login Successful");
            }
        }
    }
}

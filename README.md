## Exam - MySql Database
### User Login Authentication Module for Banking Application.
Problem Statement (Use MySQL for Database):
Develop a simple login authentication module for User which comprises of the following features:

**Login Module:**

Capture login details with the following screen fields:
- User ID
- User Password
- Authenticate login credentials with the database.
- If authentication get successful, then show “Login Successful” or else “Login Failed”.

Design hint:
- User ID (varchar) and User Password (varchar)

**Project Requirement:**
1) Create a Database.
2) Insert records into to the table.
3) Write dbConnection.properties file.
4) Write DBUtility class and establish the Database Connection.
5) Write DAO interface and DAO Implementation class.
6) Write Service Interface and Service Implementation class.
7) Ask User Login Details and handle exceptions.
8) Create a User class as POJO class.
9) Create a controller class and call the Service Implementation class.
10) Call controller class from your main application.
